/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import 'react-native-gesture-handler';
import * as React from 'react';
import NavBar from './react/components/Navbar';
import UserProvider from "./react/UserProvider";
import { StyleSheet } from "react-native";

const App: () => React$Node = () => {
  // const test= useContext(UserContext);
  // console.log('test',test)
  const user = '';
  return (
    <>
      {/*<StatusBar barStyle="dark-content" />*/}

      {/*<SafeAreaView>*/}
      <UserProvider>
       <NavBar/>
      </UserProvider>
      {/*</SafeAreaView>*/}
    </>
  );
};



const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

export default App;
