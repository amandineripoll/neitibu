import * as React from 'react';
import LoginsScreen from "../screens/Login";
import AddApplicationScreen from "../screens/AddApplication";
import HomeScreen from "../screens/Home";
import AddOfferScreen from "../screens/recruiter/AddOffer";
import RegisterScreen from "../screens/Register";
import {useContext} from "react";
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import {createStackNavigator} from '@react-navigation/stack';
import  {UserContext} from "../UserProvider";

const Tab = createBottomTabNavigator();
const HomeStack = createStackNavigator();

function HomeStackScreen() {
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen name="Home" component={HomeScreen}/>
      <HomeStack.Screen name="Apply" component={AddApplicationScreen}/>
    </HomeStack.Navigator>
  );
}

const Navbar = () => {
  const { user } = useContext(UserContext);

  return (
    <NavigationContainer>
      <Tab.Navigator
        initialRouteName="Home"
        activeColor="#f0edf6"
        inactiveColor="#3e2465"
        barStyle={{backgroundColor: '#694fad'}}
      >
          <>

            {
              !Object.keys(user).length ?
                <>
                <HomeStack.Screen name="Login" component={LoginsScreen} />
                <HomeStack.Screen name="Register" component={RegisterScreen} />
                  </>:
                <>
                  <Tab.Screen name="Home" component={HomeStackScreen}/>
                  <Tab.Screen name="Apply" component={AddApplicationScreen}/>
                  <Tab.Screen name="Offre" component={AddOfferScreen}/>
                </>
            }

          </>

      </Tab.Navigator>
    </NavigationContainer>
  );
};

export default Navbar;
