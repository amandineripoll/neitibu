import * as React from 'react';
import { TextInput, RadioButton, Text } from 'react-native-paper';
import { View, Button } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

const AddOffer = ({ navigation }) => {
  const [title, setTitle] = React.useState('');
  const [companyName, setCompanyName] = React.useState('');
  const [companyDesc, setCompanyDesc] = React.useState('');
  const [description, setDescription] = React.useState('');
  const [contractType, setContractType] = React.useState('CDI');
  const [place, setPlace] = React.useState('');
  const [jwt, setJwt] = React.useState('');

  React.useEffect(() => {
    const getJwt = async () => await AsyncStorage.getItem('token');
    setJwt(getJwt());
  }, []);

  const onSubmit = async () => {
    const configAPI = {
      method: 'POST',
      headers: {
        accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${jwt["_55"]}`
      },
      body: JSON.stringify(
        {
          title,
          companyName,
          companyDesc,
          description,
          contractType,
          place,
          creator: "/users/3",
          applications: []
        })
    }

    fetch('https://cryptic-earth-27366.herokuapp.com/offers', configAPI)
      .then((response) => response.json())
      .then((json) => {
        navigation.navigate("Home");
        console.log(json);
      })
  }

  return (
    <>
      <TextInput
        label="Titre de l'offre"
        value={title}
        onChangeText={text => setTitle(text)}
      />
      <TextInput
        label="Nom de l'entreprise"
        value={companyName}
        onChangeText={text => setCompanyName(text)}
      />
      <TextInput
        label="Description de l'entreprise"
        value={companyDesc}
        onChangeText={text => setCompanyDesc(text)}
      />
      <TextInput
        label="Description de l'offre"
        value={description}
        onChangeText={text => setDescription(text)}
      />
      <TextInput
        label="Emplacement de l'offre"
        value={place}
        onChangeText={text => setPlace(text)}
      />
      <Text>Type de contrat :</Text>
      <View>
        <Text>CDI</Text>
        <RadioButton
          value="CDI"
          status={contractType === 'CDI' ? 'checked' : 'unchecked'}
          onPress={() => setContractType('CDI')}
        />
        <Text>CDD</Text>
        <RadioButton
          value="CDD"
          status={contractType === 'CDD' ? 'checked' : 'unchecked'}
          onPress={() => setContractType('CDD')}
        />
      </View>
      <Button
        title="Créer une offre"
        onPress={onSubmit}
      />
    </>
  );
}

export default AddOffer;
