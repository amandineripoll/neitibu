import * as React from "react";
import { View, Button } from "react-native";
import { TextInput } from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';

const AddApplication = ({ navigation }) => {
  const [motivationText, setMotivationText] = React.useState('');
  const [salary, setSalary] = React.useState('');
  const [offer, setOffer] = React.useState('');
  const [candidate, setCandidate] = React.useState('');
  const [resume, setResume] = React.useState('');
  const [error, setError] = React.useState('');
  const [jwt, setJwt] = React.useState('');

  React.useEffect(() => {
    const getJwt = async () => await AsyncStorage.getItem('token');
    setJwt(getJwt());
  }, []);

  const configAPI = {
    method: 'POST',
    headers: {
      'accept': 'application/ld+json',
      'Content-Type': 'application/ld+json',
      'Authorization': `Bearer ${jwt["_55"]}`
    },
    body: JSON.stringify(
      {
        'motivationText': motivationText, 
        'salary': parseInt(salary),
        'status': 'CREATED',
        'offer': "/offers/1",
        "candidate" : "/users/23",
        "resume": ""
    })
  }

  /*const toggleFile = async () =>{
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.pdf],
      });
      console.log(
        res.uri, //uri to the file
        res.type, // mime type
        res.name,
        res.size
      );
      RNFetchBlob.fetch('POST', `https://${res.uri}`, {
        // dropbox upload headers
        Authorization : `Bearer ${token}`,
        'Dropbox-API-Arg': JSON.stringify({
          path : `/${res.name}`,
          mode : 'add',
          autorename : true,
          mute : false
        }),
        'Content-Type' : 'application/octet-stream',
        // Change BASE64 encoded data to a file path with prefix `RNFetchBlob-file://`.
        // Or simply wrap the file path with RNFetchBlob.wrap().
      })
  //setResume(base64String);
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  }

*/

  const onSubmit = async () => {

    fetch('https://cryptic-earth-27366.herokuapp.com/applications', configAPI)
      .then((response) => { 
        // console.log(response)
        if(response.status === 201) {
          return response.json();
        } else {
            console.log("JWT")
            console.log(jwt["_55"])
          setError('Application échouée');
          throw "Application échouée";
        }
      })
      .then(() => {
        navigation.navigate("Home");
      })
  }

  return (
    <View>
      <TextInput
        label="Motivation"
        value={motivationText}
        onChangeText={motivationText => setMotivationText(motivationText)}
      />
      <TextInput
        label="Salaire"
        value={salary}
        onChangeText={salary => setSalary(salary)}
      />
      <Button
        title="Postuler"
        onPress={onSubmit}
      />
    </View>
  );
};

export default AddApplication;
