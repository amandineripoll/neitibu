import React,{ useContext } from "react";
import { View, Button } from "react-native";
import { TextInput, Title, Text } from 'react-native-paper';
import { UserContext } from '../UserProvider';
import jwtDecode from 'jwt-decode';
import AsyncStorage from '@react-native-community/async-storage';

const Login = ({ navigation }) => {
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [error, setError] = React.useState('');

  const { setUser } = useContext(UserContext);

  const configAPI = {
    method: 'POST',
    headers: {
      'accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(
      {
        'email': email,
        'password': password,
    })
  }

  const storeData = async (value) => {
    try {
      await AsyncStorage.setItem('token', value)
      setUser(jwtDecode(value));
    } catch (e) {
       console.log('error')
    }
  }

  const onSubmit = async () => {

    fetch('https://cryptic-earth-27366.herokuapp.com/authentication_token', configAPI)
      .then((response) => {
        // console.log(response)
        if(response.status === 200) {
          return response.json();
        } else {
          setError('Login incorrecte');
          throw "Login incorrecte";
        }
      })
      .then((json) => {
        storeData(json.token);
        // navigation.navigate("Home");
      })
  }

  return (
    <View>
      <Title style={{textAlign: "center"}}>Connexion</Title>
      <TextInput
        label="Email"
        value={email}
        onChangeText={email => setEmail(email)}
      />
      <TextInput
        secureTextEntry
        label="Mot de passe"
        value={password}
        onChangeText={password => setPassword(password)}
      />
      <Text style={{color: "red"}}>{ !!error && error }</Text>
      <Button
        title="Connexion"
        onPress={onSubmit}
      />
      <Button
        title="Pas de compte?"
        onPress={() => navigation.navigate('Register')}
      />
    </View>
  );
};

export default Login;
