import * as React from "react";
import { View, Button } from "react-native";
import { TextInput, RadioButton, Text } from 'react-native-paper';
import { TextInputMask } from 'react-native-masked-text'
import DocumentPicker from 'react-native-document-picker';


const Register = ({ navigation }) => {
  const [firstName, setFirstName] = React.useState('');
  const [lastName, setLastName] = React.useState('');
  const [birthDate, setBirthDate] = React.useState('');
  const [sex, setSex] = React.useState('');
  const [city, setCity] = React.useState('');
  const [address, setAddress] = React.useState('');
  const [zipCode, setZipCode] = React.useState('');
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');


  const configAPI = {
  method: 'POST',
  body: JSON.stringify(
    {"email": email, 
    "password": password,
    "isVerified": true,
    "firstname": firstName, 
    "lastname": lastName, 
    "birthdate": birthDate, 
    "sexe" :true, 
    "address" : address,
    "city": city,
    "zipcode": parseInt(zipCode),
    "offers": [],
    "isRecruter": false,
    "applications": []
    
    }),
  headers: {
    'accept': 'application/json',
    'Content-Type': 'application/json'
    },
  };

  const saveUser = () => {
      fetch('https://cryptic-earth-27366.herokuapp.com/users', configAPI)
      .then((response) => response.json())
      .then((json) => {
        navigation.navigate("Login")
    })
  }
  
  return (
    <View>
      <TextInput
        label="Prénom"
        value={firstName}
        onChangeText={firstName => setFirstName(firstName)}
      />
      <TextInput
        label="Nom"
        value={lastName}
        onChangeText={lastName => setLastName(lastName)}
      />
      <TextInputMask
      type='flat'
        label="Date de naissance"
        type={'datetime'}
        options={{
          format: 'DD-MM-YYYY'
        }}
        placeholder="Date de naissance"
        value={birthDate}
        onChangeText={text => {
          setBirthDate(text)
        }}
      />
      <TextInput
        label="Email"
        value={email}
        onChangeText={email => setEmail(email)}
      />
      <TextInput
        secureTextEntry
        type="password"
        label="Mot de passe"
        value={password}
        onChangeText={password => setPassword(password)}
      />
      <Text>Homme</Text>
      <RadioButton
        value="true"
        status={sex === 'true' ? 'checked' : 'unchecked'}
        onPress={() => { setSex('true') }}
      />
      <Text>Femme</Text>
      <RadioButton
        value="false"
        status={sex === 'false' ? 'checked' : 'unchecked'}
        onPress={() => {setSex('false') }}
      />
      <TextInput
        type="Textarea"
        label="Adresse"
        value={address}
        onChangeText={address => setAddress(address)}
      />
      <TextInput
        label="Ville"
        value={city}
        onChangeText={city => setCity(city)}
      />
      <TextInput
        label="Code postal"
        value={zipCode}
        onChangeText={zipCode => setZipCode(zipCode)}
        keyboardType={'numeric'}
      />
      <Button
        title="Inscription"
        onPress={saveUser}
      />
    </View>
  );
};

export default Register;
