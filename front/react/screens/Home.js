import * as React from "react";
import { View, Button } from "react-native";
import { Card, Title, Paragraph, Text } from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';


const Home = ({ navigation }) => {
  const [offers, setOffers] = React.useState([]);

  React.useEffect(() => {
    const getJwt = async () => await AsyncStorage.getItem('token');
    const configAPI = {
      method: 'GET',
      headers: {
        'accept': 'application/ld+json',
        'Content-Type': 'application/ld+json',
        'Authorization': `Bearer ${getJwt()['_55']}`
      }
    }
    fetch('https://cryptic-earth-27366.herokuapp.com/offers', configAPI)
      .then((response) => response.json())
      .then((json) => {
          console.log(json);
          setOffers(json)
      });
  }, []);

  return (
    <View style={{ flex: 1, alignItems: "center" }}>
      <Card style={{width: '100%'}}>
        <Card.Content>
          {offers["hydra:member"] ? offers["hydra:member"].forEach(offer => {
            <>
              <Title>{offer.companyName}</Title>
              <Paragraph>Description de l'offre : {offer.description}</Paragraph>
            </>
          }) : <Text>No offers</Text>}
        </Card.Content>
      </Card>
    </View>
  );
};

export default Home;
