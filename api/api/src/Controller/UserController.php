<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;

class UserController
{
    private $managerRegistry;

    public function __construct(ManagerRegistry $managerRegistry)
    {
        $this->managerRegistry = $managerRegistry;
    }

    public function __invoke(Request $data)
    {
      $token = $data->get('token');
      $user = $this->managerRegistry->getRepository(User::class)->findOneBy(array('token' => $token));

      if (!$user) {
          return 'No user found';
      }

      $user->setIsVerified(true);
      $user->setToken(null);
      $this->managerRegistry->getManager()->flush();

      return $data;
    }
}
