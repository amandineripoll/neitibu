<?php
namespace App\EventSubscriber;
use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

final class MailerSubscriber implements EventSubscriberInterface
{
    private $mailer;

    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['verifyAccount', EventPriorities::POST_WRITE],
        ];
    }

    public function verifyAccount(ViewEvent $event): void
    {
        $user = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();
        if (!$user instanceof User || Request::METHOD_POST !== $method) {
            return;
        }

        $message = (new \Swift_Message('Inscription sur Neitibu'))
            ->setFrom('neitibu@gmail.com')
            ->setTo($user->getEmail())
            ->setBody(sprintf('Votre inscription sera validée quand vous aurez cliqué sur ce <a href="%s/%s">lien</a> de confirmation', 'https://cryptic-earth-27366.herokuapp.com/verifyAccount', $user->getToken()));

        $this->mailer->send($message);
    }
}
