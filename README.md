# Neitibu

## Front

`yarn start`
 puis dans un autre terminal `yarn android` (ne pas oublier de lancer l'émulateur sur Android studio s'il ne se lance pas automatiquement)

## Back
`docker-compose up -d --build`

### Faire une entity
`docker-compose exec php bin/console make:entity`

### Lancer une update de la base de données 
`docker-compose exec php bin/console d:s:u --force`